package io.openliberty.guides.rest;

import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.persistence.EntityManager;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;

import model.Client;
import util.HibernateUtil;

@Path("clients")
public class PropertiesResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Client getClientes() {

        System.out.println("chegou aqui");

        EntityManager session = HibernateUtil.getSessionFactory();

        System.out.println("aqui também");

        // Criteria criteria = session.createCriteria(Client.class);
        // List<Client> clients = (Client) criteria.uniqueResult();

        Client client = session.find(Client.class, 1);

        System.out.println(client);

       return client;
    }
}
