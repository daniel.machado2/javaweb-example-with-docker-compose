package org.example;

import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.Json;

// tag::Path[]
@Path("clients")
// end::Path[]
public class PropertiesResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject getClientes() {

        JsonObjectBuilder builder = Json.createObjectBuilder();

        // System.getProperties()
        //       .entrySet()
        //       .stream()
        //       .forEach(entry -> builder.add((String)entry.getKey(),
        //                                     (String)entry.getValue()));
        builder.add((String) "ok", true);

       return builder.build();
    }
}
